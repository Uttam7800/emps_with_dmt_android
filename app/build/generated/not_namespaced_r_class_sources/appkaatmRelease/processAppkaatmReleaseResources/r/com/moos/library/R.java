/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.moos.library;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int animateType = 0x7f04002a;
        public static final int circleBroken = 0x7f040060;
        public static final int corner_radius = 0x7f04008c;
        public static final int end_color = 0x7f0400a7;
        public static final int end_progress = 0x7f0400a8;
        public static final int isFilled = 0x7f0400db;
        public static final int isGraduated = 0x7f0400dc;
        public static final int isTracked = 0x7f0400de;
        public static final int progressDuration = 0x7f04016b;
        public static final int progressTextColor = 0x7f04016c;
        public static final int progressTextSize = 0x7f04016d;
        public static final int progressTextVisibility = 0x7f04016e;
        public static final int scaleZone_corner_radius = 0x7f040177;
        public static final int scaleZone_length = 0x7f040178;
        public static final int scaleZone_padding = 0x7f040179;
        public static final int scaleZone_width = 0x7f04017a;
        public static final int start_color = 0x7f040191;
        public static final int start_progress = 0x7f040192;
        public static final int textMovedEnable = 0x7f0401bf;
        public static final int text_padding_bottom = 0x7f0401c0;
        public static final int trackColor = 0x7f0401dd;
        public static final int track_width = 0x7f0401e0;
    }
    public static final class color {
        private color() {}

        public static final int blue_end = 0x7f060021;
        public static final int blue_start = 0x7f060022;
        public static final int colorAccent = 0x7f060032;
        public static final int colorPrimary = 0x7f060039;
        public static final int colorPrimaryDark = 0x7f06003a;
        public static final int dark_orange = 0x7f06004c;
        public static final int default_track_color = 0x7f060055;
        public static final int green_end = 0x7f060078;
        public static final int green_start = 0x7f060079;
        public static final int light_orange = 0x7f060082;
        public static final int purple_end = 0x7f0600c9;
        public static final int purple_start = 0x7f0600ca;
        public static final int red_end = 0x7f0600cc;
        public static final int red_start = 0x7f0600cd;
    }
    public static final class dimen {
        private dimen() {}

        public static final int default_corner_radius = 0x7f0702fb;
        public static final int default_horizontal_text_size = 0x7f0702fc;
        public static final int default_progress_text_size = 0x7f0702fd;
        public static final int default_trace_width = 0x7f0702fe;
        public static final int default_zone_corner_radius = 0x7f0702ff;
        public static final int default_zone_length = 0x7f070300;
        public static final int default_zone_padding = 0x7f070301;
        public static final int default_zone_width = 0x7f070302;
    }
    public static final class id {
        private id() {}

        public static final int AccelerateDecelerateInterpolator = 0x7f090001;
        public static final int AccelerateInterpolator = 0x7f090002;
        public static final int DecelerateInterpolator = 0x7f090004;
        public static final int LinearInterpolator = 0x7f090007;
        public static final int OvershootInterpolator = 0x7f090009;
    }
    public static final class string {
        private string() {}

        public static final int app_name = 0x7f0e0094;
        public static final int fragment_name_circle_progress = 0x7f0e00f7;
        public static final int fragment_name_horizontal_progress = 0x7f0e00f8;
        public static final int hello_blank_fragment = 0x7f0e0107;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] CircleProgressView = { 0x7f04002a, 0x7f040060, 0x7f0400a7, 0x7f0400a8, 0x7f0400db, 0x7f0400dc, 0x7f0400de, 0x7f04016b, 0x7f04016c, 0x7f04016d, 0x7f04016e, 0x7f040177, 0x7f040178, 0x7f040179, 0x7f04017a, 0x7f040191, 0x7f040192, 0x7f0401dd, 0x7f0401e0 };
        public static final int CircleProgressView_animateType = 0;
        public static final int CircleProgressView_circleBroken = 1;
        public static final int CircleProgressView_end_color = 2;
        public static final int CircleProgressView_end_progress = 3;
        public static final int CircleProgressView_isFilled = 4;
        public static final int CircleProgressView_isGraduated = 5;
        public static final int CircleProgressView_isTracked = 6;
        public static final int CircleProgressView_progressDuration = 7;
        public static final int CircleProgressView_progressTextColor = 8;
        public static final int CircleProgressView_progressTextSize = 9;
        public static final int CircleProgressView_progressTextVisibility = 10;
        public static final int CircleProgressView_scaleZone_corner_radius = 11;
        public static final int CircleProgressView_scaleZone_length = 12;
        public static final int CircleProgressView_scaleZone_padding = 13;
        public static final int CircleProgressView_scaleZone_width = 14;
        public static final int CircleProgressView_start_color = 15;
        public static final int CircleProgressView_start_progress = 16;
        public static final int CircleProgressView_trackColor = 17;
        public static final int CircleProgressView_track_width = 18;
        public static final int[] HorizontalProgressView = { 0x7f04002a, 0x7f04008c, 0x7f0400a7, 0x7f0400a8, 0x7f0400de, 0x7f04016b, 0x7f04016c, 0x7f04016d, 0x7f04016e, 0x7f040191, 0x7f040192, 0x7f0401bf, 0x7f0401c0, 0x7f0401dd, 0x7f0401e0 };
        public static final int HorizontalProgressView_animateType = 0;
        public static final int HorizontalProgressView_corner_radius = 1;
        public static final int HorizontalProgressView_end_color = 2;
        public static final int HorizontalProgressView_end_progress = 3;
        public static final int HorizontalProgressView_isTracked = 4;
        public static final int HorizontalProgressView_progressDuration = 5;
        public static final int HorizontalProgressView_progressTextColor = 6;
        public static final int HorizontalProgressView_progressTextSize = 7;
        public static final int HorizontalProgressView_progressTextVisibility = 8;
        public static final int HorizontalProgressView_start_color = 9;
        public static final int HorizontalProgressView_start_progress = 10;
        public static final int HorizontalProgressView_textMovedEnable = 11;
        public static final int HorizontalProgressView_text_padding_bottom = 12;
        public static final int HorizontalProgressView_trackColor = 13;
        public static final int HorizontalProgressView_track_width = 14;
    }
}
