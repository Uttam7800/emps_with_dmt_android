package com.iserveu.aepsStagging.utils;


/**
 * This class represents the constants used in the app.
 *
 * @author Subhalaxmi Panda
 * @date 23/06/18.
 */
public class Constants {
    /**
     * Base Url of the server
     */

//    public static final String BASE_URL = "https://iserveu.online/";
    public static final String BASE_URL = "http://35.200.225.69:8080";
    public static final int REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE = 1003;
    public static final int REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE = 1001;
    public static final int REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE = 1002;
    public static final int BALANCE_RELOAD = 1004;
    public static final String IIN_KEY = "IIN_KEY";
    public static final String TRANSACTION_STATUS_KEY = "TRANSACTION_STATUS_KEY";
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String NEXT_FRESHNESS_FACTOR = "NEXT_FRESHNESS_FACTOR";
    public static final String EASY_AEPS_PREF_KEY = "EASY_AEPS_PREF_KEY";
    public static final String EASY_AEPS_USER_LOGGED_IN_KEY = "EASY_AEPS_USER_LOGGED_IN_KEY";
    public static final String EASY_AEPS_USER_NAME_KEY = "EASY_AEPS_USER_NAME_KEY";
    public static final String EASY_AEPS_PASSWORD_KEY = "EASY_AEPS_PASSWORD_KEY";
    public static final String TEST_USER_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTI5OTkyMDQxNTE1LCJleHAiOjE1MzAwMjgwNDF9.Se0zZhaoemx6eeAVlE_9N_LqkhylDgsJS8f1-5Y4uIYIfFV_7Gst5JhwnA-ogjs4whk9x9VnNvBL8T-AVkctnQ";

}
