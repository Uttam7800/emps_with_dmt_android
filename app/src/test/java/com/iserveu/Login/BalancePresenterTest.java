/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;


import com.iserveu.aeps.dashboard.BalanceContract;
import com.iserveu.aeps.dashboard.BalancePresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link BalancePresenterTest}.
 */
public class BalancePresenterTest {



    /**
     *  Mock of BalanceContract.View for unit testing
     */
    @Mock
    private BalanceContract.View balanceView;


    /**
     *  Mock of BalancePresenter for unit testing
     */

    @InjectMocks
    private BalancePresenter balancePresenter;



    /**
     *  setupBalancePresenterTest will be called before execution of the tests
     */

    @Before
    public void setupBalancePresenterTest() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        balancePresenter = new BalancePresenter(balanceView);


    }


    /**
     * emptyTokenErrorUi() checks data whether the token is empty String
     */


    @Test
    public void emptyTokenErrorUi() {
        // When the presenter is asked to load balance with empty token
        balancePresenter.loadBalance("");

        balanceView.emptyLogin();
        // Then an empty field error is shown in the UI
        verify(balanceView).emptyLogin();



    }

    /**
     * nullTokenErrorUi() checks data whether the token is null
     */


    @Test
    public void nullTokenErrorUi() {
        // When the presenter is asked to load balance with null token
        balancePresenter.loadBalance(null);

        balanceView.emptyLogin();
        // Then an empty field error is shown in the UI
        verify(balanceView).emptyLogin();



    }

    /**
     * balanceNullErrorUi() checks data whether it is successfully posted and balance is null
     */



    @Test
    public void balanceNullErrorUi() {
        // When the presenter is asked to load balance with null balance
        balancePresenter.loadBalance("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTMwMDE2Nzc3NTk5LCJleHAiOjE1MzAwNTI3Nzd9.RNN7l23LekQtOnLRY6z62K4YPUfcdH31ufdKk8rENn3sbsO0PsFr9EHkeAY7KqyUDTlkpY06d0zS7nQBKKJIZg");

        balanceView.showBalance(null);

        // Then an empty field error is shown in the UI
        verify(balanceView).showBalance(null);



    }



    /**
     * balanceEmptyErrorUi() checks data whether it is successfully posted and balance is empty String
     */


    @Test
    public void balanceEmptyErrorUi() {
        // When the presenter is asked to load balance with empty balance
        balancePresenter.loadBalance("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTMwMDE2Nzc3NTk5LCJleHAiOjE1MzAwNTI3Nzd9.RNN7l23LekQtOnLRY6z62K4YPUfcdH31ufdKk8rENn3sbsO0PsFr9EHkeAY7KqyUDTlkpY06d0zS7nQBKKJIZg");
        balanceView.showBalance("");

        // Then an empty field error is shown in the UI
        verify(balanceView).showBalance("");



    }


    /**
     * balanceSuccessUi() checks data whether it is successfully posted and balance is fetched
     */


    @Test
    public void balanceSuccessUi() {
        // When the presenter is asked to load balance with dummy success data
        balancePresenter.loadBalance("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTMwMDE2Nzc3NTk5LCJleHAiOjE1MzAwNTI3Nzd9.RNN7l23LekQtOnLRY6z62K4YPUfcdH31ufdKk8rENn3sbsO0PsFr9EHkeAY7KqyUDTlkpY06d0zS7nQBKKJIZg");
        // When show balance in UI

        balanceView.showBalance("223077.00");

        // Then show balance in Test UI
        verify(balanceView).showBalance("223077.00");
    }
}
