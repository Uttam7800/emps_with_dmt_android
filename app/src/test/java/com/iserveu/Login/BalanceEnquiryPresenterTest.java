/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;


import com.iserveu.aeps.balanceenquiry.BalanceEnquiryContract;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryPresenter;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryRequestModel;
import com.iserveu.aeps.balanceenquiry.BalanceEnquiryResponse;
import com.iserveu.aeps.utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link BalanceEnquiryPresenterTest}.
 */
public class BalanceEnquiryPresenterTest {



    /**
     *  Mock of BalanceEnquiryContract.View for unit testing
     */
    @Mock
    private BalanceEnquiryContract.View balanceEnquiryContractView;



    /**
     *  Mock of BalanceEnquiryPresenter for unit testing
     */
    @InjectMocks
    private BalanceEnquiryPresenter balanceEnquiryPresenter;



    /**
     *  setupBalanceEnquiryPresenterTest will be called before execution of the tests
     */

    @Before
    public void setupBalanceEnquiryPresenterTest() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        balanceEnquiryPresenter = new BalanceEnquiryPresenter(balanceEnquiryContractView);


    }


    /**
     *  emptyTokenErrorUi() checks whether token is empty string  or not
     */
    @Test
    public void emptyTokenErrorUi() {
        // When the presenter is asked to check balance enquiry with null request and empty token
        balanceEnquiryPresenter.performBalanceEnquiry("",null);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }
    /**
     *  nullTokenErrorUi() checks whether token is null or not
     */
    @Test
    public void nullTokenErrorUi() {
        // When the presenter is asked to check balance enquiry with null request and token
        balanceEnquiryPresenter.performBalanceEnquiry(null,null);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullRequest() checks whether request model is null or not
     */

    @Test
    public void nullRequest() {
        // When the presenter is asked to check balance enquiry with null request
        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,null);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyAadharNoRequest() checks whether aadhar no is empty string or not
     */

    @Test
    public void emptyAadharNoRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like aadhar no
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullAadharNoRequest() checks whether aadhar no is null or not
     */
    @Test
    public void nullAadharNoRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like aadhar no
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1",null,"20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyDcRequest() checks whether dc is empty string or not
     */

    @Test
    public void emptyDcRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like dc
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullDcRequest() checks whether dc is null or not
     */
    @Test
    public void nullDcRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like dc
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230",null,"0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyciRequest() checks whether ci is empty string or not
     */

    @Test
    public void emptyCiRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like ci
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullCiRequest() checks whether ci is null or not
     */
    @Test
    public void nullCiRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like ci
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387",null,"e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyDpidRequest() checks whether dpid is empty string or not
     */

    @Test
    public void emptyDpidRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like dpid
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  nullDpidRequest() checks whether dpid is null or not
     */
    @Test
    public void nullDpidRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like dpid
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332",null,"MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyEncryptedPidRequest() checks whether EncryptedPid is empty string or not
     */

    @Test
    public void emptyEncryptedPidRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like EncryptedPid
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullEncryptedPidRequest() checks whether EncryptedPid is null or not
     */
    @Test
    public void nullEncryptedPidRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like EncryptedPid
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL",null,"-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyFreshnessFactorRequest() checks whether Freshness Factor is empty string or not
     */

    @Test
    public void emptyFreshnessFactorRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like Freshness Factor
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullFreshnessFactorRequest() checks whether Freshness Factor is null or not
     */
    @Test
    public void nullFreshnessFactorRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like Freshness Factor
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh",null,"2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyhMacRequest() checks whether hMac is empty string or not
     */

    @Test
    public void emptyhMacRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like hMac
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullhMacRequest() checks whether hMac is null or not
     */
    @Test
    public void nullhMacRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like hMac
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559",null,"607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyIinRequest() checks whether iin is empty string or not
     */

    @Test
    public void emptyIinRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like iin
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullIinRequest() checks whether iin is null or not
     */
    @Test
    public void nullIinRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like iin
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF",null,"MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyMcdataRequest() checks whether mcdata is empty string or not
     */

    @Test
    public void emptyMcdataRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like mcdata
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullMcdataRequest() checks whether mcdata is null or not
     */
    @Test
    public void nullMcdataRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like mcdata
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152",null,"MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  emptyMiRequest() checks whether mi is empty string or not
     */

    @Test
    public void emptyMiRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like mi
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  nullMiRequest() checks whether mi is null or not
     */
    @Test
    public void nullMiRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like mi
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK",null,"7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  emptyRdsIdRequest() checks whether rdsid is empty string or not
     */

    @Test
    public void emptyRdsIdRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like rdsis
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  nullRdsIdRequest() checks whether rdsid is null or not
     */
    @Test
    public void nullRdsIdRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like rdsid
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW",null,"1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  emptyRdsVErRequest() checks whether rdsver is empty string or not
     */

    @Test
    public void emptyRdsVErRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like rdsver
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  nullRdsVerRequest() checks whether rdsver is null or not
     */
    @Test
    public void nullRdsVerRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like rdsver
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001",null,"rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  emptySkeyRequest() checks whether skey is empty string or not
     */

    @Test
    public void emptySkeyRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like skey
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  nullSkeyRequest() checks whether skey is null or not
     */
    @Test
    public void nullSkeyRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like skey
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0",null);

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();

    }

    /**
     *  emptyMobileNoRequest() checks whether mobilenumber is empty or not
     */

    @Test
    public void emptyMobileNoRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like mobile
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }

    /**
     *  nullMobileNoRequest() checks whether mobilenumber is null or not
     */

    @Test
    public void nullMobileNoRequest() {
        // When the presenter is asked to check balance enquiry with null field(s) like mobile
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100",null,"WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();



    }


    /**
     *  emptyAadharAndMobileEmptyRequest() checks whether aadhar and mobile no both are  empty or not
     */

    @Test
    public void emptyAadharAndMobileEmptyRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like aadhar no , mobile number
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();
    }

    /**
     *  emptyAadharAndFreshnessFactorEmptyRequest() checks whether aadhar and freshness factor both are  empty or not
     */

    @Test
    public void emptyAadharAndFreshnessFactorEmptyRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like aadhar no , freshness factor
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();
    }

    /**
     *  emptyAadharAndIinEmptyRequest() checks whether aadhar and Bank iin number both are  empty or not
     */

    @Test
    public void emptyAadharAndIinEmptyRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like aadhar no , Bank iin number
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();
    }

    /**
     *  emptyAadharAndIinEmptyRequest() checks whether aadhar and Skey value both are  empty or not
     */

    @Test
    public void emptyAadharAndSkeyEmptyRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like aadhar no , Skey value
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();
    }

    /**
     *  EmptyRequest() checks whether  amount , aadhar no  , mobile number , bank Iin number , and fingerprints pid data  are  empty or not
     */

    @Test
    public void EmptyRequest() {
        // When the presenter is asked to check balance enquiry with empty field(s) like aadhar no, mobile number, bank iin number, freshness factor , and the fingerprint data
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("","","","","","","","","","","","","","","","","");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);

        // Then an empty field error is shown in the UI
        verify(balanceEnquiryContractView).checkEmptyFields();
    }

    /**
     *  successRequest() checks data is successfully posted
     */

    @Test
    public void successRequest() {
        // When the presenter is asked to load balance enquiry with dummy success data
        BalanceEnquiryRequestModel balanceEnquiryRequestModel = new BalanceEnquiryRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        balanceEnquiryPresenter.performBalanceEnquiry(Constants.TEST_USER_TOKEN,balanceEnquiryRequestModel);
        // When show Success message in  UI

        BalanceEnquiryResponse balanceEnquiryResponse = new BalanceEnquiryResponse ();
        balanceEnquiryContractView.checkBalanceEnquiryStatus("0","Successful", balanceEnquiryResponse);
        // Then show Success message in Test UI
        verify(balanceEnquiryContractView).checkBalanceEnquiryStatus("0","Successful", balanceEnquiryResponse);

    }
}
