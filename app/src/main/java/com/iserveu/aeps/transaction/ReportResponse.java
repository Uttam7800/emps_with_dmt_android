package com.iserveu.aeps.transaction;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportResponse {


    @SerializedName("aepsreportList")
    private ArrayList<ReportModel> aepsreportList;

    public ArrayList<ReportModel> getAepsreportList() {
        return aepsreportList;
    }

    public void setAepsreportList(ArrayList<ReportModel> aepsreportList) {
        this.aepsreportList = aepsreportList;
    }

    public ReportResponse() {
    }


}
