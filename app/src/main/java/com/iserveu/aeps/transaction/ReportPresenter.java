package com.iserveu.aeps.transaction;

import android.util.Log;

import com.iserveu.aeps.utils.AEPSAPIService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class ReportPresenter implements ReportContract.UserActionsListener {
    /**
     * Initialize ReportContractView
     */
    private ReportContract.View reportView;
    private AEPSAPIService aepsapiService;
    private ArrayList<ReportResponse> reportResponseArrayList ;
    /**
     * Initialize ReportPresenter
     */
    public ReportPresenter(ReportContract.View reportView) {
        this.reportView = reportView;
    }

    @Override
    public void loadReports(String fromDate,String toDate,String token) {

        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
            reportView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }


            final ReportAPI reportAPI =
                    this.aepsapiService.getClient().create(ReportAPI.class);

            Call<ReportResponse> call = reportAPI.insertUser(token,new ReportRequest(fromDate,toDate));


            call.enqueue(new Callback<ReportResponse>() {
                @Override
                public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                /*response.body(); // have your all data
                String userName = response.body().getStatus();*/
                    if(response.isSuccessful()) {

                        ReportResponse reportResponse = response.body();
                        Log.v("Laxmi","hfh"+reportResponse);

                        if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                            ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                            double totalAmount = 0;
                            for(int i = 0; i<result.size(); i++) {
                                totalAmount += Double.parseDouble(result.get(i).getAmount());
                            }
                            reportView.reportsReady(result, String.valueOf(totalAmount));
                        }
                    }
                    reportView.hideLoader();
                    reportView.showReports();
                }

                @Override
                public void onFailure(Call<ReportResponse> call, Throwable t) {

                    reportView.hideLoader();
                    reportView.showReports();

                }
            });

        } else {
            reportView.emptyDates();

            //loginView.checkEmptyFields();
        }
    }



    /**
     *  load Reports of  ReportActivity
     */


}
