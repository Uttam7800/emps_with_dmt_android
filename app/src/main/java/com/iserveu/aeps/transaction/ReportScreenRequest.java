package com.iserveu.aeps.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This class handles request body of Login Module from login Api
 *
 * @author Subhalaxmi Panda
 * @date 23/06/18.
 */

public class ReportScreenRequest {

    /**
     * request body parameters for login Api
     */
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
