package com.iserveu.aeps.fundtransferreport;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class CustomReportAdapter extends RecyclerView.Adapter<CustomReportAdapter.ViewHolder> implements Filterable {

    private FundTransferReport context;
    private List<FinoTransactionReports> finoTransactionReports;
    private List<FinoTransactionReports> finoTransactionReportsFiltered;
    public CustomReportAdapter(FundTransferReport context , List finoTransactionReports) {
        this.context = context;
        this.finoTransactionReports = finoTransactionReports;
        this.finoTransactionReportsFiltered = finoTransactionReports;
    }




    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fund_transfer_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.itemView.setTag(finoTransactionReportsFiltered.get(position));

        FinoTransactionReports pu = finoTransactionReportsFiltered.get(position);

        holder.transactionId.setText(pu.getId());
        holder.accountNumber.setText(pu.getToAccount());
        holder.apiTid.setText(pu.getApiTid());
        holder.apiComment.setText(pu.getApiComment());
        holder.previousAmount.setText(pu.getPreviousAmount());
        holder.amountTransacted.setText(pu.getAmountTransacted());
        holder.currentBalAmt.setText(pu.getBalanceAmount());
        holder.bankName.setText(pu.getBankName());
        holder.beniMobile.setText(pu.getBeniMobile());
        holder.benificiaryName.setText(pu.getBenificiaryName());
        holder.transactionMode.setText(pu.getTransactionMode());
        holder.transactionType.setText(pu.getTransactionType());
        holder.createdDate.setText(Util.getDateFromTime(Long.parseLong(pu.getCreatedDate())));
        holder.gatewaY.setText(pu.getRouteID());
        holder.statuS.setText(pu.getStatus());
        holder.updatedDate.setText(Util.getDateFromTime(Long.parseLong(pu.getUpdatedDate())));
        if (pu.getStatus().equals("SUCCESS")) {
            holder.cardViewFundtransferReport.setBackgroundColor(ContextCompat.getColor(context, R.color.color_report_green));
        }else {
            holder.cardViewFundtransferReport.setBackgroundColor(ContextCompat.getColor(context, R.color.color_report_red));
        }

    }

    @Override
    public int getItemCount() {
        return finoTransactionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView transactionId,accountNumber,apiTid,apiComment,previousAmount,amountTransacted,currentBalAmt,bankName,beniMobile,benificiaryName,transactionMode,transactionType,createdDate,gatewaY,statuS,updatedDate;

        public CardView cardViewFundtransferReport;
//        String date;

        public ViewHolder(View itemView) {
            super(itemView);

            cardViewFundtransferReport = itemView.findViewById(R.id.card_view_fundtransfer_report);


            transactionId = (TextView) itemView.findViewById(R.id.transaction_id);
            accountNumber = (TextView) itemView.findViewById(R.id.account_number);
            apiTid = (TextView) itemView.findViewById(R.id.api_tid);
            apiComment = (TextView) itemView.findViewById(R.id.api_comment);
            previousAmount = (TextView) itemView.findViewById(R.id.previous_amount);
            amountTransacted = (TextView) itemView.findViewById(R.id.amount_transacted);
            currentBalAmt = (TextView) itemView.findViewById(R.id.current_bal_amt);
            bankName = (TextView) itemView.findViewById(R.id.bank_name);
            beniMobile = (TextView) itemView.findViewById(R.id.beni_mobile);
            benificiaryName = (TextView) itemView.findViewById(R.id.benificiary_name);
            transactionMode = (TextView) itemView.findViewById(R.id.transaction_mode);
            transactionType = (TextView) itemView.findViewById(R.id.transaction_type);
            createdDate = (TextView) itemView.findViewById(R.id.created_date);
            gatewaY = (TextView) itemView.findViewById(R.id.gateway);
            statuS = (TextView) itemView.findViewById(R.id.status);
            updatedDate = (TextView) itemView.findViewById(R.id.updated_date);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    finoTransactionReportsFiltered = finoTransactionReports;
                } else {
                    List<FinoTransactionReports> filteredList = new ArrayList<>();
                    for (FinoTransactionReports row : finoTransactionReports) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getApiTid().toLowerCase().contains(charString.toLowerCase()) || row.getApiTid().contains(charSequence)) {
                            filteredList.add(row);
                        }

                        if (row.getId().toLowerCase().contains(charString.toLowerCase()) || row.getId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    finoTransactionReportsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = finoTransactionReportsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                finoTransactionReportsFiltered = (ArrayList<FinoTransactionReports>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
