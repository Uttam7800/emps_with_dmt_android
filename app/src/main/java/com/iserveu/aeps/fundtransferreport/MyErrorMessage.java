package com.iserveu.aeps.fundtransferreport;

public class MyErrorMessage {
    private String statusDesc;

    public String getMessage() {
        return statusDesc;
    }

    public void setMessage(String statusDesc) {
        this.statusDesc = statusDesc;
    }
}
