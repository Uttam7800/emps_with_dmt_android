package com.iserveu.aeps.microatm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.finopaytech.finosdk.encryption.AES_BC;
import com.finopaytech.finosdk.helpers.Utils;
import com.iserveu.aeps.R;
import com.iserveu.aeps.aeps2.Aeps2Activity;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.login.LoginActivity;
import com.iserveu.aeps.matmtransaction.MatmSettlementActivity;
import com.iserveu.aeps.matmtransaction.MicroAtmReportActivity;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.iserveu.aeps.utils.Util.showProgress;

public class MicroAtmActivity extends AppCompatActivity implements View.OnClickListener,MicroAtmContract.View {

    public TextView aepsTabOption,labelTextiew,aeps2Option,matmTabOption;
    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry;
    EditText etAmount;
    Button btnProceed;
    String strTransType = "";
    String encData;
    String authentication;
    public MicroAtmPresenter microAtmPresenter;
    MicroAtmRequestModel microAtmRequestModel;
    LoadingView loadingView;
    MicroAtmTransactionModel microAtmTransactionModel;
    LinearLayout tabLayout;
    ArrayList<String> featurescodesresponse;
    Session session;
    Button blutoothButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_micro_atm);

        setToolbar();
        microAtmPresenter = new MicroAtmPresenter(this);
        session = new Session(MicroAtmActivity.this);
        matmTabOption = findViewById( R. id.matmTabOption);
        matmTabOption.setVisibility(View.GONE);
        aepsTabOption = findViewById( R. id.aepsTabOption);
        aepsTabOption.setVisibility(View.GONE);
        aeps2Option = findViewById( R. id.aeps2Option);
        aeps2Option.setVisibility(View.GONE);
        rgTransactionType = findViewById(R.id.rg_trans_type);
        rbCashWithdrawal = findViewById(R.id.rb_cw);
        rbBalanceEnquiry = findViewById(R.id.rb_be);
        btnProceed = findViewById(R.id.btn_proceed);
        etAmount = findViewById(R.id.et_amount);
        labelTextiew = findViewById(R.id.labelTextiew);
        tabLayout = findViewById(R.id.tabLayout);
        blutoothButton = findViewById(R.id.blutoothButton);

        hideKeyboard();

        microAtmPresenter.loadFeature(session.getUserToken());
        aeps2Option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MicroAtmActivity.this, Aeps2Activity.class);
                startActivity(in);
                finish();
            }
        });
        blutoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingIntent = new Intent(MicroAtmActivity.this, DeviceSettingActivity.class);
                settingIntent.putExtra("IS_PAIR_DEVICE",true);
                startActivityForResult(settingIntent,3);
            }
        });
        rgTransactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {

                    case R.id.rb_cw:
                        etAmount.setClickable(true);
                        etAmount.setHint("Amount");
                        etAmount.setVisibility(View.VISIBLE);
                        labelTextiew.setVisibility(View.VISIBLE);
                        labelTextiew.setText("Enter Amount Below");
                        strTransType = "Cash Withdrawal";
                        etAmount.setText("");
                        etAmount.setEnabled(true);
                        etAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;

                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        etAmount.setVisibility(View.GONE);
                        labelTextiew.setVisibility(View.GONE);
                        etAmount.setClickable(false);
                        etAmount.setEnabled(false);
                        break;
                }
            }
        });
        btnProceed.setOnClickListener(this);

        aepsTabOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MicroAtmActivity.this, DashboardActivity.class);
                startActivity(in);
                finish();
            }
        });

    }

    private void setToolbar() {

        Toolbar mToolbar = findViewById ( R.id.toolbar );
        mToolbar.setTitle ( getResources ().getString ( R.string.home_title ));
        mToolbar.inflateMenu ( R.menu.matm_menu );

        mToolbar.setOnMenuItemClickListener ( new Toolbar.OnMenuItemClickListener () {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.action_report){
                    Intent in = new Intent(MicroAtmActivity.this,MicroAtmReportActivity.class);
                    startActivity(in);
                }else if(item.getItemId()== R.id.action_logout){
                    if (session!=null){
                        session.clear();
                        Intent in = new Intent(MicroAtmActivity.this,LoginActivity.class);
                        startActivity(in);
                        finish();
                    }
                }else if(item.getItemId() == R.id.action_settlement){
                    Intent in = new Intent(MicroAtmActivity.this,MatmSettlementActivity.class);
                    startActivity(in);
                }
                return false;
            }
        } );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                showLoader();
                if(Utils.isMicroATMDevicePaired(MicroAtmActivity.this) == true) {
                    if (validate()) {
                        if (rbCashWithdrawal.isChecked()) {
                            apiCalling();
                        } else if (rbBalanceEnquiry.isChecked()) {
                            balanceEnquiryApiCalling();
                        }
                    }
                }else{
                    hideLoader();
                    Util.showAlert(this,getResources().getString(R.string.alert_error),getResources().getString(R.string.connection_bluetooth_device));
                }
                break;
        }
    }

    private boolean validate() {
        if ((!rbCashWithdrawal.isChecked()) && (!rbBalanceEnquiry.isChecked())) {
            showOneBtnDialog(this, "Info", "Please select Transaction Type!", false);
            return false;
        }
        if(rbCashWithdrawal.isChecked()) {
            if (etAmount.getText().toString().equals("")) {
                String msg = "";
                if (rbCashWithdrawal.isChecked()) {
                    msg = "Amount";
                }
                hideLoader();
                showOneBtnDialog(this, "Info", "Please enter " + msg + " !", false);
                return false;
            }
        }
        return true;
    }

    private void showOneBtnDialog(final Context mContext, String title, String msg, boolean cancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                hideKeyboard();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(cancelable);
        dialog.show();
    }

    public void hideKeyboard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String getServiceID() {
        String clientRefID = "";


        if (rbCashWithdrawal.isChecked())
            clientRefID = Constants.SERVICE_MICRO_CW;
        if (rbBalanceEnquiry.isChecked())
            clientRefID = Constants.SERVICE_MICRO_BE;

        return clientRefID;
    }


    public void apiCalling()
    {
        microAtmRequestModel = new MicroAtmRequestModel(etAmount.getText().toString(),getServiceID(),"mobile");
        microAtmPresenter.performRequestData(session.getUserToken(), microAtmRequestModel);

    }

    public void balanceEnquiryApiCalling()
    {
        microAtmRequestModel = new MicroAtmRequestModel("0",getServiceID(),"mobile");
        microAtmPresenter.performRequestData(session.getUserToken(), microAtmRequestModel);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null & resultCode == RESULT_OK) {
            microAtmTransactionModel = new MicroAtmTransactionModel();
            if (requestCode == 1) {
                String response;
                if (data.hasExtra("ClientResponse")) {
                    hideLoader();
                    response = data.getStringExtra("ClientResponse");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String strDecryptResponse = AES_BC.getInstance().decryptDecode(Utils.replaceNewLine(response), Constants.CLIENT_REQUEST_ENCRYPTION_KEY);
                            Log.v("radha", "respones transaction : " + strDecryptResponse);
                            if (rbCashWithdrawal.isChecked()) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                microAtmTransactionModel.setTxnStatus(jsonObject.getString("TxnStatus"));
                                microAtmTransactionModel.setTxnAmt(jsonObject.getString("TxnAmt"));
                                microAtmTransactionModel.setRnr(jsonObject.getString("RRN"));
                                microAtmTransactionModel.setCardNumber(jsonObject.getString("CardNumber"));
                                microAtmTransactionModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                microAtmTransactionModel.setTransactionDateandTime(jsonObject.getString("TransactionDatetime"));
                                microAtmTransactionModel.setTerminalId(jsonObject.getString("TerminalID"));
                                microAtmTransactionModel.setType(getServiceID());
                                Log.v("radha", "cashwithdrawl enquiry : " + strDecryptResponse);
                            } else if (rbBalanceEnquiry.isChecked()) {
                                JSONObject jsonObject = new JSONObject(strDecryptResponse);
                                microAtmTransactionModel.setBalanceEnquiryStatus(jsonObject.getString("BalanceEnquiryStatus"));
                                microAtmTransactionModel.setRnr(jsonObject.getString("RRN"));
                                microAtmTransactionModel.setCardNumber(jsonObject.getString("CardNumber"));
                                microAtmTransactionModel.setAvailableBalance(jsonObject.getString("AvailableBalance"));
                                microAtmTransactionModel.setTransactionDateandTime(jsonObject.getString("TransactionDatetime"));
                                microAtmTransactionModel.setAccountNo(jsonObject.getString("AccountNo"));
                                microAtmTransactionModel.setTerminalId(jsonObject.getString("TerminalID"));
                                microAtmTransactionModel.setType(getServiceID());
                                Log.v("radha", "balance enquiry : " + strDecryptResponse);
                            }

                        } catch (Exception e) {
                            microAtmTransactionModel = null;
                            Log.v("test", "error : " + e);
                        }
                    }
                } else if (data.hasExtra("ErrorDtls")) {
                    hideLoader();
                    response = data.getStringExtra("ErrorDtls");
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            String[] error_dtls = response.split("\\|");
                            String errorMsg = error_dtls[0];
                            microAtmTransactionModel.setErrormsg(errorMsg);
                        } catch (Exception exp) {
                            microAtmTransactionModel = null;
                            Log.v("test", "Error : " + exp.toString());
                        }
                    }
                } else {
                    microAtmTransactionModel = null;
                }
            }
            if (requestCode == 3) {
                String response;
                if (data.hasExtra("DeviceConnectionDtls")) {
                    response = data.getStringExtra("DeviceConnectionDtls");
                    String[] error_dtls = response.split("\\|");
                    String errorMsg = error_dtls[0];
                    String errorMsg2 = error_dtls[1];
                    microAtmTransactionModel.setErrormsg(errorMsg2);
                    microAtmTransactionModel.setStatusError(errorMsg);
                    Log.v("radha","device response : "+response);

                }
            }

            nextPage();
        }
    }

    public void nextPage(){

        Intent intentAtm = new Intent(MicroAtmActivity.this,MATMTransactionStatusActivity.class);
        intentAtm.putExtra(Constants.MICRO_ATM_TRANSACTION_STATUS_KEY,microAtmTransactionModel);
        startActivity(intentAtm);

    }
/*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK)
            return true;

        return true;
    }*/

    @Override
    public void checkRequestCode(String status, String message, MicroAtmResponse microAtmResponse) {
        if(status!= null && !status.matches("")) {
            authentication = microAtmResponse.getAuthentication();
            encData = microAtmResponse.getEncData();

            Intent intent = new Intent(getBaseContext(), com.finopaytech.finosdk.activity.MainTransactionActivity.class);
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);// Application return time in second
            startActivityForResult(intent, 1);
        }else{
            Util.showAlert(this,getResources().getString(R.string.fail_error),message);
        }
    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
       String aepsFeatureCode = "27";
        String matmFeatureCode = "33";
        String aeps2FeatureCode = "38";
       /* String aepsFeatureCode = "30";
        String matmFeatureCode = "40";
        String aeps2FeatureCode = "38";*/
        ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aepsFeatureCode);
        featureCode.add(matmFeatureCode);
        featureCode.add(aeps2FeatureCode);
        featurescodesresponse = new ArrayList<>();

        for (int i = 0; i < userFeatures.size(); i++) {

            if(featureCode.contains(userFeatures.get(i).getId())) {
                if(aepsFeatureCode.equalsIgnoreCase(userFeatures.get(i).getId())){
                    aepsTabOption.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(aepsFeatureCode);
                }
                if(matmFeatureCode.equalsIgnoreCase(userFeatures.get(i).getId())){
                    matmTabOption.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(matmFeatureCode);
                }
                if(aeps2FeatureCode.equalsIgnoreCase(userFeatures.get(i).getId())){
                    aeps2Option.setVisibility(View.VISIBLE);
                    featurescodesresponse.add(aeps2FeatureCode);
                }
            } else {
                Log.v("radha", "not contains : "+userFeatures.get(i).getId());
            }
        }
        if(featurescodesresponse.size() == 2){
            tabLayout.setWeightSum(2);
        }else if(featurescodesresponse.size() == 3){
            tabLayout.setWeightSum(3);
        }else{
            tabLayout.setWeightSum(1);
        }
    }

    @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = showProgress(MicroAtmActivity.this);
        }
        loadingView.show();
    }

    @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
    }
}
