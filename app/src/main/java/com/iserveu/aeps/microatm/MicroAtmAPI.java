package com.iserveu.aeps.microatm;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MicroAtmAPI {
    @POST("/sendEncryptedRequestNFS")
    Call<MicroAtmResponse> checkRequestCode(@Header("Authorization") String token, @Body MicroAtmRequestModel body);
}
