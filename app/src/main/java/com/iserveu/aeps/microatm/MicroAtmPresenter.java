package com.iserveu.aeps.microatm;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.iserveu.aeps.dashboard.BalanceApi;
import com.iserveu.aeps.dashboard.BalanceResponse;
import com.iserveu.aeps.utils.AEPSAPIService;

import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MicroAtmPresenter implements MicroAtmContract.UserActionsListener {

    private MicroAtmContract.View microAtmContractView;
    private AEPSAPIService aepsapiService;

    public MicroAtmPresenter(MicroAtmContract.View microAtmContractView) {
        this.microAtmContractView = microAtmContractView;
    }

    @Override
    public void performRequestData(String token, final MicroAtmRequestModel microAtmRequestModel) {

        if(microAtmRequestModel!=null && microAtmRequestModel.getAmount()!=null && microAtmRequestModel.getTransactionType()!=null
                && !microAtmRequestModel.getTransactionType().matches("") && microAtmRequestModel.getTransactionMode()!=null && !microAtmRequestModel.getTransactionMode().matches(""))
        {
            microAtmContractView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            MicroAtmAPI microAtmAPI =this.aepsapiService.getClient().create(MicroAtmAPI.class);

            microAtmAPI.checkRequestCode(token,microAtmRequestModel).enqueue(new Callback<MicroAtmResponse>() {
                @Override
                public void onResponse(Call<MicroAtmResponse> call, Response<MicroAtmResponse> response) {
                    if(response.isSuccessful()) {
                        // String message = "";
                        if (response.body().getAuthentication()!=null && !response.body().getAuthentication().matches("")) {
                            //message = "Login Successful";
                            microAtmContractView.hideLoader();
                            microAtmContractView.checkRequestCode(response.body().getAuthentication(), response.body().getEncData(),response.body());
                        }else{
                            microAtmContractView.hideLoader();
                            microAtmContractView.checkRequestCode("", "Encryption Failed",null);
                            Log.v("laxmi","hf"+microAtmContractView);
                        }

                    } else {
                        if(response.errorBody() != null) {
                            JsonParser parser = new JsonParser();
                            JsonElement mJson = null;
                            try {
                                mJson = parser.parse(response.errorBody().string());
                                Gson gson = new Gson();
                                MicroAtmResponse errorResponse = gson.fromJson(mJson, MicroAtmResponse.class);
                                microAtmContractView.hideLoader();
                                microAtmContractView.checkRequestCode("",errorResponse.getErrorResponse(),null);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }else{
                            microAtmContractView.hideLoader();
                            microAtmContractView.checkRequestCode("","Service is currently unavailable",null);
                        }
                    }
                }

                @Override
                public void onFailure(Call<MicroAtmResponse> call, Throwable t) {

                    microAtmContractView.hideLoader();
                    microAtmContractView.checkRequestCode("", "Encrryption Failed",null);

                }
            });
          //  {"BalanceEnquiryStatus":"Balance Enquiry Successful","RRN":"900515029759","CardNumber":"**********956444","AvailableBalance":"29924.68","TransactionDatetime":"2019-01-05 15:41:56","AccountNo":"50100109092763","TerminalID":"IS000008"}


        } else {
            microAtmContractView.hideLoader();
            microAtmContractView.checkEmptyFields();
        }

    }

    @Override
    public void loadFeature(String token) {
        if (token!=null) {

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            microAtmContractView.showLoader ();
            // this.aepsapiService = new AEPSAPIService();

            BalanceApi balanceApi = this.aepsapiService.getClient().create(BalanceApi.class);
            Call<BalanceResponse> respuesta = balanceApi.getBalanceDetails(token,"");
            respuesta.enqueue(new Callback<BalanceResponse>() {
                @Override
                public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                    if (response.isSuccessful()){
                        String json = response.body().getUserInfoModel().getUserBrand();

                        try {

                            JSONObject obj = new JSONObject(json);

                            if (obj.has("brand")){
                                response.body().getUserInfoModel().setUserBrand(obj.getString("brand"));
                            }

                            //balanceView.showBalance(response.body().getUserInfoModel().getUserBrand());


                        } catch (Throwable t) {
                        }
                        microAtmContractView.hideLoader ();
                        microAtmContractView.showFeature(response.body().getUserInfoModel().getFeatureIdList());
                    }

                }

                @Override
                public void onFailure(Call<BalanceResponse> call, Throwable t) {
                    microAtmContractView.hideLoader ();
                }


            });

        }
    }
}
