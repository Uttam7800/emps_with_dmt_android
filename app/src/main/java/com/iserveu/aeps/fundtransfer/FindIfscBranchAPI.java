package com.iserveu.aeps.fundtransfer;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Url;

public interface FindIfscBranchAPI {

    @GET()
    // @FormUrlEncoded
//    @Headers("Content-Type: application/json")
    Call<List<IfscListModel>> getStatus(@Header("Authorization") String token, @Url String url);
//    Call<List<Datum>> getJSON();
}
