package com.iserveu.aeps.matmtransaction;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface RefreshApi {

    @POST("/sendEncryptedRequestTransactionEnquiryMATM")
    Call<RefreshModel> insertUser(@Header("Authorization") String token, @Body RefreshRequest body);
}
