package com.iserveu.aeps.matmtransaction;

import android.util.Log;

import com.iserveu.aeps.utils.AEPSAPIService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MicroReportPresenter implements MicroReportContract.UserActionsListener {

    /**
     * Initialize ReportContractView
     */
    private MicroReportContract.View microReportContractView;
    private AEPSAPIService aepsapiService;
    private ArrayList<MicroReportModel> microReportModelArrayList ;
    /**
     * Initialize ReportPresenter
     */
    public MicroReportPresenter(MicroReportContract.View microReportContractView) {
        this.microReportContractView = microReportContractView;
    }

    @Override
    public void loadReports(String fromDate, String toDate, String token, String transactionType) {
        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
            microReportContractView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }


            MicroReportApi reportAPI =
                    this.aepsapiService.getClient().create(MicroReportApi.class);

            Call<MicroReportResponse> call = reportAPI.insertUser(token,new MicroReportRequest(fromDate,toDate,transactionType));

            call.enqueue(new Callback<MicroReportResponse>() {
                @Override
                public void onResponse(Call<MicroReportResponse> call, Response<MicroReportResponse> response) {
                    if(response.isSuccessful()) {

                        MicroReportResponse reportResponse = response.body();
                        if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                            ArrayList<MicroReportModel> result = reportResponse.getmATMTransactionReport();
                            double totalAmount = 0;
                            for(int i = 0; i<result.size(); i++) {
                                totalAmount += Double.parseDouble(result.get(i).getAmountTransacted());
                            }
                            microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                        }
                    }
                    microReportContractView.hideLoader();
                    microReportContractView.showReports();
                }

                @Override
                public void onFailure(Call<MicroReportResponse> call, Throwable t) {
                    microReportContractView.hideLoader();
                    microReportContractView.showReports();
                }
            });
        } else {
            microReportContractView.emptyDates();
        }
    }

    @Override
    public void refreshReports(String token, String amount, String transactionType, String transactionMode, String clientUniqueId) {
        microReportContractView.showLoader();
        if(amount == null || amount.matches("")){
            microReportContractView.checkAmount("1");
            return;
        }
        if(transactionMode ==null || transactionMode.matches("")){
            microReportContractView.checkTransactionMode("1");
            return;
        }
        if(transactionType == null || transactionType.matches("")){
            microReportContractView.checkTransactionType("1");
            return;
        }
        if(clientUniqueId == null || clientUniqueId.matches("")){
            microReportContractView.checkClientId("1");
            return;
        }

        RefreshApi reportAPI =
                this.aepsapiService.getClient().create(RefreshApi.class);

        Call<RefreshModel> call = reportAPI.insertUser(token,new RefreshRequest(amount,transactionType,transactionMode,clientUniqueId));

        call.enqueue(new Callback<RefreshModel>() {
            @Override
            public void onResponse(Call<RefreshModel> call, Response<RefreshModel> response) {

                if(response.isSuccessful()){
                    RefreshModel refreshModel = response.body();
                    if (refreshModel != null) {
                        microReportContractView.hideLoader();
                        microReportContractView.refreshDone(refreshModel);
                    }
                }else{
                    microReportContractView.hideLoader();
                    microReportContractView.emptyRefreshData("1");
                }
            }

            @Override
            public void onFailure(Call<RefreshModel> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.emptyRefreshData("1");
            }
        });

    }


    /**
     *  load Reports of  ReportActivity
     */


}