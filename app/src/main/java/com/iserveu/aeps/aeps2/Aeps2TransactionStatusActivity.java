package com.iserveu.aeps.aeps2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Constants;

public class Aeps2TransactionStatusActivity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aeps2_transaction_status);
        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(Constants.AEPS_2_TRANSACTION_STATUS_KEY) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occured");
        }else{
            Aeps2ResponesModel aeps2ResponesModel = (Aeps2ResponesModel) getIntent().getSerializableExtra(Constants.AEPS_2_TRANSACTION_STATUS_KEY);

            if (aeps2ResponesModel .getTransactionType() !=null && aeps2ResponesModel.getTransactionType().matches("cashwithdrawl") ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                String txnstatus = "NA";
                if(aeps2ResponesModel .getStatus() !=null && !aeps2ResponesModel .getStatus().matches("")){
                    txnstatus = aeps2ResponesModel.getStatus();
                }

                String balance = "NA";
                if(aeps2ResponesModel.getAvailableBalance() !=null && !aeps2ResponesModel.getAvailableBalance().matches("NA")){
                    balance = aeps2ResponesModel.getAvailableBalance();
                }

                String adhaarnumber = "NA";
                if(aeps2ResponesModel.getAdhaarNo() != null && !aeps2ResponesModel.getAdhaarNo().matches("")){
                    adhaarnumber = aeps2ResponesModel.getAdhaarNo();
                }
                String rferencesnumber = "NA";
                if(aeps2ResponesModel.getrRN()!= null && !aeps2ResponesModel.getrRN().matches("")){
                    rferencesnumber = aeps2ResponesModel.getrRN();
                }

                String data = "NA";
                if(aeps2ResponesModel.getTxnDate() !=null && !aeps2ResponesModel.getTxnDate().matches("")){
                    data = aeps2ResponesModel.getTxnDate();
                }

                String amount = "NA";
                if(aeps2ResponesModel.getAmount() !=null && !aeps2ResponesModel.getAmount().matches("")){
                    amount = aeps2ResponesModel.getAmount();
                }

                String time = "NA";
                if(aeps2ResponesModel.getTxnTime() !=null && !aeps2ResponesModel.getTxnTime().matches("")){
                    time = aeps2ResponesModel.getTxnTime();
                }

                String bankName = "NA";
                if(aeps2ResponesModel.getBankName() !=null && !aeps2ResponesModel.getBankName().matches("")){
                    bankName = aeps2ResponesModel.getBankName();
                }

                String mobileNumber = "NA";
                if(aeps2ResponesModel.getCustomerMobile() !=null && !aeps2ResponesModel.getCustomerMobile().matches("")){
                    mobileNumber = aeps2ResponesModel.getCustomerMobile();
                }

                String ledgerBalance = "NA";
                if(aeps2ResponesModel.getLedgerBalance() !=null && !aeps2ResponesModel.getLedgerBalance().matches("")){
                    ledgerBalance = aeps2ResponesModel.getLedgerBalance();
                }

                if(aeps2ResponesModel.getType() !=null && !aeps2ResponesModel.getType().matches("")){
                    detailsTextView.setText ( " Cash Withdrawal for customer account linked with aadhar card " + adhaarnumber + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + rferencesnumber + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount + "\n Transaction Date : " +data + "\n Transaction Time : " +time);
                }

            }else  if (aeps2ResponesModel .getTransactionType() !=null && aeps2ResponesModel.getTransactionType().matches("balance") ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                String txnstatus = "NA";
                if(aeps2ResponesModel .getStatus() !=null && !aeps2ResponesModel .getStatus().matches("")){
                    txnstatus = aeps2ResponesModel.getStatus();
                }

                String balance = "NA";
                if(aeps2ResponesModel.getAvailableBalance() !=null && !aeps2ResponesModel.getAvailableBalance().matches("NA")){
                    balance = aeps2ResponesModel.getAvailableBalance();
                }

                String adhaarnumber = "NA";
                if(aeps2ResponesModel.getAdhaarNo() != null && !aeps2ResponesModel.getAdhaarNo().matches("")){
                    adhaarnumber = aeps2ResponesModel.getAdhaarNo();
                }
                String rferencesnumber = "NA";
                if(aeps2ResponesModel.getrRN()!= null && !aeps2ResponesModel.getrRN().matches("")){
                    rferencesnumber = aeps2ResponesModel.getrRN();
                }

                String data = "NA";
                if(aeps2ResponesModel.getTxnDate() !=null && !aeps2ResponesModel.getTxnDate().matches("")){
                    data = aeps2ResponesModel.getTxnDate();
                }

                String amount = "NA";
                if(aeps2ResponesModel.getAmount() !=null && !aeps2ResponesModel.getAmount().matches("")){
                    amount = aeps2ResponesModel.getAmount();
                }

                String time = "NA";
                if(aeps2ResponesModel.getTxnTime() !=null && !aeps2ResponesModel.getTxnTime().matches("")){
                    time = aeps2ResponesModel.getTxnTime();
                }

                String bankName = "NA";
                if(aeps2ResponesModel.getBankName() !=null && !aeps2ResponesModel.getBankName().matches("")){
                    bankName = aeps2ResponesModel.getBankName();
                }

                String mobileNumber = "NA";
                if(aeps2ResponesModel.getCustomerMobile() !=null && !aeps2ResponesModel.getCustomerMobile().matches("")){
                    mobileNumber = aeps2ResponesModel.getCustomerMobile();
                }

                String ledgerBalance = "NA";
                if(aeps2ResponesModel.getLedgerBalance() !=null && !aeps2ResponesModel.getLedgerBalance().matches("")){
                    ledgerBalance = aeps2ResponesModel.getLedgerBalance();
                }

                if(aeps2ResponesModel.getType() !=null && !aeps2ResponesModel.getType().matches("")){
                    detailsTextView.setText ( " Cash Withdrawal for customer account linked with aadhar card " + adhaarnumber + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + rferencesnumber + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount + "\n Transaction Date : " +data + "\n Transaction Time : " +time);
                }
            }else if(aeps2ResponesModel.getStatusError() != null && aeps2ResponesModel.getErrormsg() != null &&
                    !aeps2ResponesModel.getStatusError().matches("") && !aeps2ResponesModel.getErrormsg().matches("") &&
                    aeps2ResponesModel.getStatusError().trim().equalsIgnoreCase("success")) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                if(aeps2ResponesModel.getErrormsg() != null && !aeps2ResponesModel.getErrormsg().matches("")){
                    detailsTextView.setText(aeps2ResponesModel.getErrormsg());
                    detailsTextView.setGravity(Gravity.CENTER);
                }
            }else if(aeps2ResponesModel.getStatusError() != null && aeps2ResponesModel.getErrormsg() != null &&
                    !aeps2ResponesModel.getStatusError().matches("") && !aeps2ResponesModel.getErrormsg().matches("") &&
                    aeps2ResponesModel.getStatusError().trim().equalsIgnoreCase("failed")) {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                if(aeps2ResponesModel.getErrormsg() != null && !aeps2ResponesModel.getErrormsg().matches("")){
                    failureDetailTextView.setText(aeps2ResponesModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again !!!");
                }
            }else {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                if(aeps2ResponesModel.getErrormsg() != null && !aeps2ResponesModel.getErrormsg().matches("")){
                    failureDetailTextView .setText (aeps2ResponesModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again after some time.");
                }
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
