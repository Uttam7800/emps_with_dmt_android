package com.iserveu.aeps.bankspinner;

import android.content.Context;

import com.iserveu.aeps.utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class BankNameListPresenter implements BankNameContract.UserActionsListener {
    /**
     * Initialize ReportContractView
     */
    private BankNameContract.View banknameView;
   // private ArrayList<BankNameModel> bankNameModelArrayList ;
    /**
     * Initialize ReportPresenter
     */
    public BankNameListPresenter(BankNameContract.View banknameView) {
        this.banknameView = banknameView;
    }



    @Override
    public void loadBankNamesList(Context context) {
        banknameView.showLoader();
        try {
            ArrayList<BankNameModel> bankNamesArrayList = new ArrayList<BankNameModel>();

            JSONArray bankNamesJsonArray = new JSONArray(Util.loadJSONFromAsset(context));
            if (bankNamesJsonArray != null) {
                for (int i=0;i<bankNamesJsonArray.length();i++){
                    JSONObject jsonObject = bankNamesJsonArray.getJSONObject(i);
                    bankNamesArrayList.add(new BankNameModel(jsonObject.getString("BANKNAME"),jsonObject.getString("IIN")));
                }
            }
            banknameView.hideLoader();
            banknameView.bankNameListReady(bankNamesArrayList);
            banknameView.showBankNames();
        } catch (JSONException e) {
            banknameView.hideLoader();
            e.printStackTrace();
        }
    }


    /**
     *  load Reports of  ReportActivity
     */


}
