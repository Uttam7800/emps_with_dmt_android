package com.iserveu.aeps.login;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.iserveu.aeps.dashboard.BalanceApi;
import com.iserveu.aeps.dashboard.BalanceResponse;
import com.iserveu.aeps.microatm.MicroAtmResponse;
import com.iserveu.aeps.utils.AEPSAPIService;
import com.iserveu.aeps.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.aeps.fundtransfer.FundTransfer.TAG;
import static com.iserveu.aeps.utils.Constants.ADMIN_NAME;
import static com.iserveu.aeps.utils.Constants.GET_LOADED_FEATURE_URL;
import static com.iserveu.aeps.utils.Constants.GET_LOGIN_DETAILS_URL;
import static com.iserveu.aeps.utils.Constants.GET_LOGIN_ENCRIPTED_URL;
import static com.iserveu.aeps.utils.Constants.USER_NAME;
import static org.apache.commons.ssl.PEMUtil.bytesToHex;

/*
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class LoginPresenter implements LoginContract.UserActionsListener {
    /*
     * Initialize LoginView
     */
    private LoginContract.View loginView;
    private AEPSAPIService aepsapiService;
    /**
     * Initialize LoginPresenter
     */
    public LoginPresenter(LoginContract.View loginView) {
        this.loginView = loginView;
    }



    /**
     *  performLogin() Checks Login button click of LoginActivity
     */

    @Override
    public void performLogin(final String username, final String password) {

        if (!username.matches("") && username != null && !password.matches("") && password != null) {
            loginView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            AndroidNetworking.get(GET_LOGIN_ENCRIPTED_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                perFormEncodedLogin(username,password,encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });





           /* GetTokenAPI GetTokenAPI = this.aepsapiService.getClient().create(GetTokenAPI.class);
                GetTokenAPI.getToken(new GetTokenRequest(username,password), Constants.GET_LOGIN_TOKEN).enqueue(new Callback<GetTokenResponse>() {
                    @Override
                    public void onResponse(Call<GetTokenResponse> call, Response<GetTokenResponse> response) {

                        if(response.isSuccessful()) {
                            // String message = "";
                            if (response.body().getToken() !=null && !response.body().getToken().matches("")) {
                                //message = "Login Successful";
                               // getLoginDetails(response.body().getToken());
                                ADMIN_NAME = response.body().getAdminName();
                                loadFeature(response.body().getToken());
                            }else{
                                loginView.hideLoader();
                                // message = "Login Failed";
                                loginView.checkLoginStatus("", "Login Failed","","");

                            }
                            // loginView.checkLoginStatus(response.body().getToken(), message);

                        }else {
                            if(response.errorBody() != null) {
                                JsonParser parser = new JsonParser();
                                JsonElement mJson = null;
                                try {
                                    mJson = parser.parse(response.errorBody().string());
                                    Gson gson = new Gson();
                                    GetTokenResponse errorResponse = gson.fromJson(mJson, GetTokenResponse.class);
                                    loginView.hideLoader();
                                    loginView.checkLoginStatus("","Login Failed","","");
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                    loginView.hideLoader();
                                    loginView.checkLoginStatus("", "Login Failed","","");
                                }
                            }else{
                                loginView.hideLoader();
                                loginView.checkLoginStatus("", "Login Failed","","");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetTokenResponse> call, Throwable t) {
                        loginView.hideLoader();
                        loginView.checkLoginStatus("", "Login Failed","","");
                    }
                });
        */
        } else {
            loginView.checkEmptyFields();
        }
    }
// created Rajesh
    public void perFormEncodedLogin(String username, String password, String encodedUrl){
        GetTokenAPI GetTokenAPI = this.aepsapiService.getClient().create(GetTokenAPI.class);
        GetTokenAPI.getToken(new GetTokenRequest(username,password), encodedUrl).enqueue(new Callback<GetTokenResponse>() {
            @Override
            public void onResponse(Call<GetTokenResponse> call, Response<GetTokenResponse> response) {

                if(response.isSuccessful()) {
                    // String message = "";
                    if (response.body().getToken() !=null && !response.body().getToken().matches("")) {

                        ADMIN_NAME = response.body().getAdminName();
                        loadFeature(response.body().getToken());
                    }else{
                        loginView.hideLoader();
                        // message = "Login Failed";
                        loginView.checkLoginStatus("", "Login Failed","","");

                    }
                    // loginView.checkLoginStatus(response.body().getToken(), message);

                }else {
                    if(response.errorBody() != null) {
                        JsonParser parser = new JsonParser();
                        JsonElement mJson = null;
                        try {
                            mJson = parser.parse(response.errorBody().string());
                            Gson gson = new Gson();
                            GetTokenResponse errorResponse = gson.fromJson(mJson, GetTokenResponse.class);
                            loginView.hideLoader();
                            loginView.checkLoginStatus("","Login Failed","","");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            loginView.hideLoader();
                            loginView.checkLoginStatus("", "Login Failed","","");
                        }
                    }else{
                        loginView.hideLoader();
                        loginView.checkLoginStatus("", "Login Failed","","");
                    }
                }
            }

            @Override
            public void onFailure(Call<GetTokenResponse> call, Throwable t) {
                loginView.hideLoader();
                loginView.checkLoginStatus("", "Login Failed","","");
            }
        });

    }

    @Override
    public void loadFeature(final String token) {
        if (token!=null) {

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            loginView.showLoader ();

            AndroidNetworking.get(GET_LOADED_FEATURE_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                callEncriptLoadedFeature(aepsapiService,token,encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }
    }

    //Created Rajesh
    public void callEncriptLoadedFeature(AEPSAPIService aepsapiService, final String token,String urlString){
        BalanceApi balanceApi = this.aepsapiService.getClient().create(BalanceApi.class);
        Call<BalanceResponse> respuesta = balanceApi.getBalanceDetails(token,urlString);
        respuesta.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                if (response.isSuccessful()){
                    String json = response.body().getUserInfoModel().getUserBrand();

                    String user_fname = response.body().getUserInfoModel().getUserProfile().getFirstName();
                    String user_lname = response.body().getUserInfoModel().getUserProfile().getLastName();
                    USER_NAME = user_fname;

                    try {

                        JSONObject obj = new JSONObject(json);

                        if (obj.has("brand")){
                            response.body().getUserInfoModel().setUserBrand(obj.getString("brand"));
                        }

                    } catch (Throwable t) {
                    }
                    loginView.hideLoader ();
                    loginView.showLoginFeature(response.body().getUserInfoModel().getFeatureIdList(),token);
                }else {
                    if(response.errorBody() != null) {
                        JsonParser parser = new JsonParser();
                        JsonElement mJson = null;
                        try {
                            mJson = parser.parse(response.errorBody().string());
                            Gson gson = new Gson();
                            BalanceResponse errorResponse = gson.fromJson(mJson, BalanceResponse.class);
                            loginView.hideLoader();
                            loginView.showLoginFeature(null,"");
                        } catch (IOException ex) {
                            loginView.hideLoader();
                            loginView.showLoginFeature(null,"");
                            ex.printStackTrace();
                        }
                    }else{
                        loginView.hideLoader();
                        loginView.showLoginFeature(null,"");
                    }
                }

            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                loginView.hideLoader ();
                loginView.showLoginFeature(null,"");
            }


        });

    }


    public void getLoginDetails(final String token){
        AndroidNetworking.get(GET_LOGIN_DETAILS_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            encriptedLoginDetails(token,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    //Edited Rajesh
    public void encriptedLoginDetails(final String token, String encodedUrl){
        LoginApi loginApi = this.aepsapiService.getClient().create(LoginApi.class);

        Call<LoginResponse> call = loginApi.insertUser(token,encodedUrl);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {

                    String message = "";
                    if (response.body().getStatus() != null && !response.body().getStatus().matches("") && response.body().getStatus().matches("0")) {
                        message = "Login Successful";
                    } else {
                        message = "Login Failed";
                    }
                    loginView.hideLoader();
                    loginView.checkLoginStatus(response.body().getStatus(), message, token, response.body().getNextFreshnessFactor());

                }else {
                    if(response.errorBody() != null) {
                        JsonParser parser = new JsonParser();
                        JsonElement mJson = null;
                        try {
                            mJson = parser.parse(response.errorBody().string());
                            Gson gson = new Gson();
                            LoginResponse errorResponse = gson.fromJson(mJson, LoginResponse.class);
                            loginView.hideLoader();
                            loginView.checkLoginStatus(errorResponse.getStatus(), errorResponse.getStatusDesc(),"","");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            loginView.hideLoader();
                            loginView.showLoginFeature(null,"");
                        }
                    }else{
                        loginView.hideLoader();
                        loginView.showLoginFeature(null,"");
                    }
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginView.checkLoginStatus("", "Login Failed","","");

            }
        });
    }

}
