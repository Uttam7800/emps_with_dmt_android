package com.iserveu.aeps.utils;

import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import java.io.IOException;

public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject> {

    private String latestVersion;
    private String currentVersion;
    private Context context;
    public ForceUpdateAsync(String currentVersion, Context context,AsyncListener asyncListener){
        this.currentVersion = currentVersion;
        this.context = context;
        this.asyncListener = asyncListener;

    }
    private AsyncListener asyncListener;

    public interface AsyncListener {
        public void doStuff(String latestVersion,String currentVersion );
    }
    @Override
    protected JSONObject doInBackground(String... params) {

        try {
            latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName()+ "&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                    .first()
                    .ownText();
        } catch (IOException e) {
            asyncListener.doStuff(latestVersion,currentVersion);
            e.printStackTrace();
        }
        return new JSONObject();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        asyncListener.doStuff(latestVersion,currentVersion);
        super.onPostExecute(jsonObject);
    }




}

